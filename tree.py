class Tree:
    def __init__(self, info, left=None, right=None):
        self.info = info
        self.left = left 
        self.right = right 

    def __str__(self):
        return str(self.info)

    def insert(self, info):
        """ 
        Insert new node into a Tree
        @param data node data object 

        """
        if info < self.info:
            
            if self.left == None:
                self.left = Tree(info)
            else:
                self.left.insert(info)
        else:
            if self.right == None:
                self.right = Tree(info)
            else:
                self.right.insert(info)

    def preorder(self):
        """ 
        traverse the tree in preorder: parent all the left descendents 
        then all the right descendents
        """
        print self
        #newparent = self;
        if self.left:
             self.left.preorder()
        if self.right:
             self.right.preorder()

    def inorder(self):
        """ 
        traverse the tree in inorder: parent all the left descendents 
        then all the right descendents
        """
        if self.left:
             self.left.inorder()
        print self
        if self.right:
             self.right.inorder()

    def postorder(self):
        """ 
        traverse the tree in postorder: parent all the left descendents 
        then all the right descendents
        """
        if self.left:
             self.left.postorder()
        if self.right:
             self.right.postorder()
        print self
    
    def stackpreorder(self):
        stack = []
        stack.append(self)
        while True:
            if stack:
                popout = stack.pop()
                print popout
                if popout.right != None:
                    stack.append(popout.right)
                if popout.left != None:
                    stack.append(popout.left)
            else:
                break
                        
    def lcpbst(self,n1,n2):
        while self != None:
            if (n1 < int(self.info)) and (n2 > int(self.info)):
                print self
                break
            else:
                if int(self.info) > n2:
                    print "self > n2", n2 
                    self = self.left 
                if int(self.info) < n1:
                    print "self < n1" , n1
                    self = self.right

    def test(self, n1, n2):
        print int(self.info) + 2

a = Tree(100)
a.insert(50)
a.insert(75)
a.insert(150)
a.insert(125)
a.insert(110)
a.insert(175)
a.insert(25)
a.insert(45)
a.insert(15)
a.insert(85)

#a.stackpreorder()
a.preorder()
a.lcpbst(15, 45)
#a.test(15, 75)
a.lcpbst(25, 75)



