def partition(arr, lower , upper):
    if upper - lower <= 1:
       if arr[upper] < arr[lower]:
         temp = arr[lower]
         arr[lower] = arr[upper]
         arr[upper] = temp
       return arr
    inc = (upper  - lower) / 2 
    mid = lower + inc

    
    partition(arr, lower, mid)
    partition(arr, mid+1, upper)
    
    aux = []
    merge(arr, aux, lower, mid, upper)
    
    return arr 

def mergesort(arr, lo , upper):
    partition(arr, lo, upper )
    return arr
    
def merge(arr, aux, lo, mid, upper):
    
    aux = [None] * len(arr) 
    for i in xrange(0, len(arr)):
        aux[i] = arr[i]

    k = lo;
    l = mid + 1
    
    for j in xrange(lo, upper+1 ):
        if k > mid :
            arr[j] = aux[l]
            l = l+1
        elif l > upper:
            arr[j] = aux[k]
            k = k + 1
        elif aux[k] > aux[l]:
            arr[j] = aux[l]
            l = l + 1
        else:
            arr[j] = aux[k]
            k = k + 1
    return arr        

def heap_sort(arr):
    build_max_heap(arr)
    n = heap_size(arr)
    a = arr
    while n>=0:
        hsort(a, n)
        n = n - 1
    print a

def hsort(a, n):
    temp = a[0] 
    a[0] = a[n]
    a[n] = temp
    a[0:n] = max_heapify(a[0:n], 0)
    return a



def build_max_heap(arr):
    i = heap_size(arr) / 2
    while (i>=0):
        arr = max_heapify(arr,i)
        i = i - 1

def heap_size(arr):
    return len(arr) -1


def max_heapify(a, i):
    l = 2*i
    r = 2*i + 1
    largest = i
    if l <= heap_size(a) and a[l] > a[largest]:
        largest = l
    if r <= heap_size(a) and a[r] > a[largest]:
        largest = r
    if largest != i:
       temp = a[i] 
       a[i] = a[largest]
       a[largest] = temp
       max_heapify(a, largest) 
    return a


def quick_partition(arr,lo,hi,pivot):
    i = lo + 1
    j = hi
    while (arr[i] < arr[pivot]) and (i <= hi):
         i = i + 1
    while ( arr[j] > arr[pivot]) and (j >= lo):
         j = j - 1
    print "i,j", i ,j
    
    if i < j:
        temp = arr[i] 
        arr[i] = arr[j]
        arr[j] = temp
        newpivot = quick_partition(arr,i,j,pivot)
    else:
        if j > pivot:
          temp = arr[pivot]
          arr[pivot] = arr[j]
          arr[j] = temp
          newpivot = j
        else:
          newpivot = pivot 
#    
#    if i > j:
#         temp = arr[pivot]
#         arr[pivot] = arr[j]
#         arr[j] = temp
#         newpivot = j
#    else:
#         temp = arr[i] 
#         arr[i] = arr[j]
#         arr[j] = temp
#         newpivot = quick_partition(arr,i,j,pivot)
    return newpivot 


def quicksort(arr, lo, hi, pivot):
   if lo < hi :
     pivot = quick_partition(arr,lo,hi,pivot)
     quicksort(arr,lo, pivot - 1, lo)
     quicksort(arr,pivot + 1, hi, pivot + 1) 
   

def insertsort(arr, n):
    for j in range(1,n+1):
        i = j
        while i > 0:
            if arr[i] < arr[i-1]:
                temp = arr[i]
                arr[i] = arr[i -1 ]
                arr[i-1] = temp
            i = i - 1
    return arr


arr = [21, 44,67,67]
#quicksort(arr, 0, len(arr) - 1, 0) 
print "insertsort", insertsort(arr,len(arr) - 1 )        
garr = [58,33,66,58,48, 48, 11,8,9,56,44,67,67,1,1]
print "insertsort", insertsort(garr,len(garr) - 1 )        
#quicksort(garr, 0, len(garr) - 1, 0) 
print garr





























#arr = [111,23, 56,43 , 56 ,23 , 55, 99, 01, 45]
#heap_sort(arr)

##arr = [ 43, 56, 23, 66]            
#print mergesort(arr, 0 , len(arr) - 1 )
#
#
#arr = [43 , 56 ,23 , 11, 3, 5, 1, 9]
#print mergesort(arr, 0 , len(arr) - 1 )
#
#arr = [56 ,23 , 11, 35, 99, 01, 33,57, 44445,67, 35]
#print mergesort(arr, 0 , len(arr) - 1 )
#
#arr = [56,45]
#print partition(arr, 0 , len(arr) - 1 )
#
#arr = [56,45,33]
#print mergesort(arr, 0 , len(arr) - 1 )
#
