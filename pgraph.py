from collections import defaultdict 
from sets import Set
import heapq


adjdfs = {}
parentdfs = {}
def dfs_visit(u,adju):
    global adjdfs
    global parentdfs
    for v in adju:
        if v not in parentdfs:
            parentdfs[v] = u
            dfs_visit(v,adjdfs[v])
    print "v completed : ", v 

def dfs(v,adj):
    global adjdfs
    adjdfs = adj
    global parentdfs
    for s in v:
        if s not in parentdfs:
            parentdfs[s] = None 
            dfs_visit(s, adjdfs[s])
    print "parentdfs ", parentdfs

# now suedocode for BFS

def bfs(s,adj):
    level = {s:0}
    i = 1
    parent={s:None}
    frontier = [s]

    while frontier:
        next = []
        for u in frontier:
            for v in adj[u]:
                if v not in level:
                    level[v] = i
                    next.append(v)
                    parent[v] = u
        frontier = next
        i = i + 1
        print level
        print parent

h = [] # heap for dijkstra
dist = {}
predecessor = {} 

def shortest_path(s,adj,t):
    print t
    while t != s :
        t = predecessor[t]
        print t

def relax(u,v,w):
    global h
    global predecessor
    global dist
    if dist[v] > dist[u] + w:
        dist[v] = dist[u] + w 
        predecessor[v] = u
        i = 0
        j = 0
        for n,m in h:
            if m == v:
                h.pop(i)
            i = i + 1
        heapq.heappush(h,(dist[v],v))


def dijkstra(adj,s):
    print "debug: input adj ", adj # debug
    for u in adj.keys():
        if u == s:
            dist[u] = 0
        else:
            dist[u] = float("inf")
    
   
    for u in adj.keys():
        heapq.heappush(h,(dist[u],u))
   
    print "debug: h before dijk ", h #debug
    
    while h:
        chkheap = defaultdict(list)
        d, u = heapq.heappop(h)
        print "debug : h while dijk", h
        
        for n, m in h :
           chkheap[m] = n  
        
        for v in adj[u].keys():
            if v in chkheap:
                print "debug: before relaxing v: ",v 
                relax(u,v,adj[u][v])
                print "debug : after each relaxation " , dist 
  
    print "debug: dist after dijk ", dist #debug
    print "debug: predecessor ", predecessor
    print "debug: h after dijk ", h #debug
   
    shortest_path(s,adj,'x')
    print " next sp"
    shortest_path(s,adj,'y')
    print " next sp"
    shortest_path(s,adj,'z')



hprim = [] # heap for dijkstra
distprim = {}
predecessorprim = {}

def mst():
    global predecessorprim
    weight = 0
    print "MST edges:"
    for key in predecessorprim.keys():
        print key, predecessorprim[key]
        weight = weight +  distprim[key]
    print "Weight of mst: ", weight


def relaxprim(u,v,w):
    global hprim
    global predecessor
    global distprim
    if distprim[v] >  w:
        distprim[v] = w 
        predecessorprim[v] = u
        i = 0
        j = 0
        for n,m in hprim:
            if m == v:
                hprim.pop(i)
            i = i + 1
        heapq.heappush(hprim,(distprim[v],v))

def prim(adj,s):
    weight = 0
    seta = Set()
    for u in adj.keys():
        if u == s:
            distprim[u] = 0
        else:
            distprim[u] = float("inf")
    
   
    for u in adj.keys():
        heapq.heappush(hprim,(distprim[u],u))
   
    
    while hprim:
        chkheap = defaultdict(list)
        d, u = heapq.heappop(hprim)
        if u != s:
            seta.add((predecessorprim[u],u))
        print "debug : hprim while prim", hprim
        
        for n, m in hprim :
           chkheap[m] = n  
        
        for v in adj[u].keys():
            if v in chkheap:
                relaxprim(u,v,adj[u][v])
  
    
    print seta
    for u,v in seta:
        weight = weight + distprim[v]

    print weight


def bf(adj,s,t):
    
    print "debug: input adj ", adj # debug
    count  = 0
    distbf = {}
    predecessorbf={}
    for u in adj.keys():
            distbf[u] = float("inf")
            count = count + 1
  
    distbf[s] = 0 
    for i in range(1,count):
     for u in adj.keys():
        for v in adj[u].keys():
           if distbf[v] > distbf[u] + adj[u][v]:
               distbf[v] = distbf[u] + adj[u][v]
               predecessorbf[v] = u
    
    print "debug : after relaxation " , distbf 
  
    for u in adj.keys():
        for v in adj[u].keys():
           if distbf[v] > distbf[u] + adj[u][v]:
                print "distbf[v] > distbf[u] + adj[u][v]:" , v,distbf[v],u,distbf[u],adj[u][v]
                print " Negative Cycle found"
                exit()

     
    print "shortest path:"
    
    print t
    while t != s :
        g = t
        t = predecessorbf[t]
        print t, distbf[g]



def kruskal(adj):
    seta = {} 
    setb = Set()
    setd = Set()
    setmem = {}
    weight = 0
    i = 0
    for u in adj.keys():
        seta[u] = Set()
        seta[u].add(u)
        i = i + 1 

    kr = []
    for u in  adj.keys():
        for v in adj[u].keys():
            heapq.heappush(kr,(adj[u][v],(u,v)))

    while kr:
        w,uv = heapq.heappop(kr)
        u,v = uv
        if (seta[u] != seta[v]):
            setb = seta[u].union(seta[v])
            setd.add((u,v))
            for m in setb:
                seta[m] = setb
            weight = weight + adj[u][v]
            
    print " weight :", weight
    print setd

adjunorderedPrim = {'a':{'b':4,'h':8},'b':{'a':4,'h':11,'c':8},'h':{'a':8,'b':11,'i':7,'g':1},'i':{'h':7, 'c':2, 'g':6},'c':{'b':8,'i':2, 'd':7, 'f':4}, 'g':{'h':1,'i':6, 'f':2}, 'f':{'g':2, 'c':4, 'd':14, 'e':10}, 'e':{'f':10, 'd':9}, 'd':{'e':9, 'f':14, 'c':7}}


adjposdigraph = {'s':{'t':10, 'y':5}, 't':{'x':1, 'y':2}, 'y':{'x':9, 't':3, 'z':2}, 'z':{'s':7, 'x':6}, 'x':{'z':4}}

#adjnegdigraph = {s:{t:6, y:7} , t:{x:5, y:8, z:-4}, y:{x:-3, z:9}, z:{s:2, x:7}, x:{t:-2}}
adjnegdigraph = {'s':{'t':6, 'y':7} , 't':{'x':-5, 'y':8, 'z':-4}, 'y':{'x':-3, 'z':9}, 'z':{'s':2, 'x':7}, 'x':{'t':-2}}

#bfs('a',adjunorderedPrim)
#dfs('a',adjunorderedPrim)
#dfs('s',adjposdigraph)
#bf(adjnegdigraph,'s','z') 
#bf(adjnegdigraph,'s','t') 
bf(adjnegdigraph,'y','t') 
#bf(adjnegdigraph,'x','t') 
#
##dijkstra(adjposdigraph,'s')
#
kruskal(adjunorderedPrim)
#prim(adjunorderedPrim,'a')
prim(adjunorderedPrim,'g')
#prim(adjunorderedPrim,'h')
#prim(adjunorderedPrim,'d')

#prim(adjunorderedPrim,'f')










